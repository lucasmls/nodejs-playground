const express = require('express')
const validateMiddleware = require('express-validation')
const handle = require('express-async-handler')

const { Ad, Purchase, Session, User } = require('./app/validations')
const authMiddleware = require('./app/middlewares/auth')

const {
  UserController,
  SessionController,
  AdController,
  PurchaseController
} = require('./app/controllers')

const routes = express.Router()

routes.post('/users', validateMiddleware(User), handle(UserController.store))
routes.post(
  '/sessions',
  validateMiddleware(Session),
  handle(SessionController.store)
)

// Todas as rotas abaixo passarão no middleware de auth
routes.use(authMiddleware)

// Ads
routes.get('/ads', handle(AdController.index))
routes.get('/ads/:id', handle(AdController.show))
routes.post('/ads', validateMiddleware(Ad), handle(AdController.store))
routes.put('/ads/:id', validateMiddleware(Ad), handle(AdController.update))
routes.delete('/ads/:id', handle(AdController.destroy))

// Purchases
routes.post(
  '/purchases',
  validateMiddleware(Purchase),
  handle(PurchaseController.store)
)

routes.patch('/purchases/accept/:id', handle(PurchaseController.accept))

module.exports = routes

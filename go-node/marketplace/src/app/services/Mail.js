const path = require('path')
const nodemailer = require('nodemailer')
const mailConfig = require('../../config/mail')

const handlebars = require('nodemailer-express-handlebars')
const expressHandlebars = require('express-handlebars')

const transport = nodemailer.createTransport(mailConfig)
transport.use(
  'compile',
  handlebars({
    viewEngine: expressHandlebars.create({ partialsDir: [] }),
    viewPath: path.resolve(__dirname, '..', 'views', 'emails'),
    extName: '.hbs'
  })
)

module.exports = transport

const Ad = require('../models/Ad')

class AdController {
  async index (req, res) {
    const filters = {}

    if (req.query.min_price || req.query.max_price) {
      filters.price = {}

      if (req.query.min_price) {
        filters.price.$gte = req.query.min_price
      }

      if (req.query.max_price) {
        filters.price.$lte = req.price.max_price
      }
    }

    if (req.query.title) {
      filters.title = new RegExp(req.query.title, 'i')
    }

    filters.purchasedBy = { $exists: false }

    // const ads = await Ad.populate('author').find()
    const ads = await Ad.paginate(filters, {
      page: +req.query.page || 1,
      limit: 10,
      sort: '-createdAr',
      populate: ['author']
    })
    return res.json(ads)
  }

  async show (req, res) {
    const ad = await Ad.findById(req.params.id)
    return res.json(ad)
  }

  async store (req, res) {
    const ad = await Ad.create({ ...req.body, author: req.userId })
    return res.json(ad)
  }

  async update (req, res) {
    const ad = await Ad.findByIdAndUpdate(req.params.id, req.body, {
      new: true
    })

    return res.json(ad)
  }

  async destroy (req, res) {
    await Ad.findByIdAndDelete(req.params.id)
    return res.json({ message: 'Ad successfully removed' })
  }
}

module.exports = new AdController()

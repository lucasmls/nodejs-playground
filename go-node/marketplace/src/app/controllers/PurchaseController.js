const Ad = require('../models/Ad')
const User = require('../models/User')
const Purchase = require('../models/Purchase')

const PurchaseMail = require('../jobs/PurchaseMail')
const Queue = require('../services/Queue')

class PurchaseController {
  async store (req, res) {
    const { ad, content } = req.body

    const purchaseAd = await Ad.findById(ad).populate('author')

    if (purchaseAd.purchasedBy) {
      return res.status(403).json({ message: 'Este produto já foi vendido' })
    }

    const user = await User.findById(req.userId)

    await Purchase.create({ ad, content, author: user.id })

    Queue.create(PurchaseMail.key, {
      ad: purchaseAd,
      user,
      content
    }).save()

    return res.json({
      message: `Solicitação de compra para o produto: ${
        purchaseAd.title
      } enviada com sucesso!`
    })
  }

  async accept (req, res) {
    const { id } = req.params
    const ad = await Ad.findById(id)

    if (!ad.author._id.equals(req.userId)) {
      return res.status(401).json({ error: 'Você não é o dono do anúncio' })
    }

    if (ad.purchasedBy) {
      return res.status(403).json({
        message:
          'Não é possível aceitar a venda de um produto já vendido anteriormente.'
      })
    }

    ad.purchasedBy = id
    await ad.save()

    res.json(ad)
  }
}

module.exports = new PurchaseController()

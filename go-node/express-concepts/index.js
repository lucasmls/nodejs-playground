const express = require('express')
const nunjucks = require('nunjucks')

const app = express()

nunjucks.configure('views', {
  autoescape: true,
  express: app,
  watch: true
})

app.use(express.urlencoded({ extended: false }))
app.set('view engine', 'njk')

const users = ['Lucas Mendes', 'Laisla Pinto Coelho', 'Daniel de Lima']
app.get('/', (req, res) => {
  res.render('list', { users })
})

app.get('/register', (req, res) => {
  res.render('register')
})

app.post('/create', (req, res) => {
  console.log(req.body)
  users.push(req.body.name)
  return res.redirect('/')
})

app.listen(3000)

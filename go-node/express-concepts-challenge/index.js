const express = require('express')
const nunjucks = require('nunjucks')

const app = express()

nunjucks.configure('views', {
  autoescape: true,
  express: app,
  watch: true
}) // Configuração da view engine

app.set('view engine', 'njk') // Definindo que usaremos o nunjucks
app.use(express.urlencoded({ extended: false })) // Fazendo com que seja possível acessar o req.body

const ageValidationMiddleware = (req, res, next) => {
  const { age } = req.query
  if (!age) {
    return res.redirect('/')
  }

  return next()
}

app.get('/', (req, res) => {
  return res.render('index')
})

app.post('/check', (req, res) => {
  const { age } = req.body
  return age < 18
    ? res.redirect(`/minor?age=${age}`)
    : res.redirect(`/major?age=${age}`)
})

app.get('/minor', ageValidationMiddleware, (req, res) => {
  const { age } = req.query
  return res.render('minor', { age })
})

app.get('/major', ageValidationMiddleware, (req, res) => {
  const { age } = req.query
  return res.render('major', { age })
})

app.listen(3000)

import { Injectable, NestMiddleware } from '@nestjs/common';
import { Request, Response } from 'express';

@Injectable()
export class LoggerMiddleware implements NestMiddleware {
  // Consider using the simpler functional middleware alternative any time your middleware doesn't need any dependencies.
  use(req: Request, res: Response, next: () => void) {
    console.log(`[${req.method}] - ${req.originalUrl}`);

    next();
  }
}

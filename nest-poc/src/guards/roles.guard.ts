import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}

  canActivate(context: ExecutionContext) {
    // Here, we access the 'admin' param that was added into the decorator
    const roles = this.reflector.get<string[]>('roles', context.getHandler());
    if (!roles) {
      // If no role is provided, we simply let the user continue with the request
      return true;
    }

    // Here we must implement some logic that checks if the user is allowed to proceed

    return true;
  }
}

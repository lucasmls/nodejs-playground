import { Module } from '@nestjs/common';

import { CatsController } from './cats.controller';
import { CatsService } from './cats.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Cat } from './cat.model';

// With @Global() decorator, any controller can access this service without importing in into your own module.
// Global modules should be registered only once, generally by the root or core module

// @Global()
@Module({
  imports: [TypeOrmModule.forFeature(
    [Cat],
  )],
  controllers: [CatsController],
  providers: [CatsService],

  // With this export, any module that imports CatsModule will
  // have access to CatsService and will share the same instance
  // with all other modules that import it as well, becaus it's a Singleton.

  // exports: [CatsService],
})
export class CatsModule {}

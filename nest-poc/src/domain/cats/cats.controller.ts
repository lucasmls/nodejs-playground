import { Controller, Get, Query, Post, Body, Put, Param, Delete, UsePipes, UseGuards, UseInterceptors } from '@nestjs/common';
import * as Joi from '@hapi/joi';

import { CatsQueryParams, CreateCatPayload, UpdateCatPayload } from './dto/cats.dto';
import { CatsService } from './cats.service';
import { JoiValidationPipe } from '../../pipes/JoiValidationPipe';
import { RolesGuard } from '../../guards/roles.guard';
import { Roles } from '../../decorators/roles.decorator';
import { TimeTakenInterceptor } from '../../interceptors/time-taken.interceptor';

const CreateCatSchema = Joi.object({
  name: Joi.string().required(),
  age: Joi.number().required(),
  breed: Joi.string().required(),
});

@Controller('cats')
@UseGuards(RolesGuard)
@UseInterceptors(TimeTakenInterceptor)
export class CatsController {

  constructor(private readonly catsService: CatsService) {}

  @Post()
  @UsePipes(new JoiValidationPipe(CreateCatSchema))
  @Roles('admin')
  public async create(@Body() payload: CreateCatPayload) {
    const cat = await this.catsService.create(payload);
    return cat;
  }

  @Get()
  public async findAll(@Query() query: CatsQueryParams) {
    const cats = await this.catsService.findAll();
    return cats;
  }

  @Get(':id')
  public async findOne(@Param('id') id: string) {
    const cat = await this.catsService.find(id);
    return cat;
  }

  @Put(':id')
  public async update(@Param('id') id: string, @Body() payload: UpdateCatPayload) {
    const cat = await this.catsService.update(id, payload);
    return cat;
  }

  @Delete(':id')
  public async remove(@Param('id') id: number) {
    await this.catsService.delete(id);
    return `Cat with id: ${id} removed successfully.`;
  }

}

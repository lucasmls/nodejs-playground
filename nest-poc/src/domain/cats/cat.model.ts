import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity('cats')
export class Cat {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('varchar', { nullable: false })
  name: string;

  @Column('integer', { nullable: false })
  age: number;

  @Column('varchar', { nullable: false })
  breed: string;
}

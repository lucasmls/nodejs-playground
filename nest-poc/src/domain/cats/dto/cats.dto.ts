export interface CatsQueryParams {
  name: string;
  limit: number;
}

export interface CreateCatPayload {
  name: string;
  age: number;
  breed: string;
}

export interface UpdateCatPayload {
  name?: string;
  age?: number;
  breed?: string;
}

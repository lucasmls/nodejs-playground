import { Injectable } from '@nestjs/common';

import { Cat as ICat } from './interfaces/Cat';
import { InjectRepository } from '@nestjs/typeorm';
import { Cat } from './cat.model';
import { Repository, UpdateResult } from 'typeorm';

@Injectable()
export class CatsService {
  private cats: ICat[] = [];

  constructor(
    @InjectRepository(Cat) private readonly catRepository: Repository<Cat>,
  ) {}

  public async findAll(): Promise<Cat[]> {
    const cats = await this.catRepository.find();
    return cats;
  }

  public async create(payload: ICat): Promise<Cat> {
    const cat = await this.catRepository.save(payload);
    return cat;
  }

  public async update(
    id: string,
    payload: { name?: string; age?: number; breed?: string },
  ): Promise<any> {
    await this.catRepository.update(id, payload);
    const cat = await this.catRepository.findOneOrFail(id);
    return cat;
  }

  public async find(id: string): Promise<ICat> {
    const cat = await this.catRepository.findOne(id);
    return cat;
  }

  public async delete(id: number): Promise<void> {
    await this.catRepository.delete(id);
  }
}

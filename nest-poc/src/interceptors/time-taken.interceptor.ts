import { Injectable, NestInterceptor, ExecutionContext, CallHandler } from '@nestjs/common';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Injectable()
export class TimeTakenInterceptor implements NestInterceptor {
  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const startTime = Date.now();

    const { name: controllerName } = context.getClass();
    const { name: handlerName } = context.getHandler();

    return next.handle().pipe(
      tap(() => {
        const endTime = Date.now();
        const timeSpent = (endTime - startTime) / 100;

        console.log(`[${controllerName}] - ${handlerName} took: ${timeSpent.toFixed(2)} seconds.`)
      }),
    );
  }
}

import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  // If we want to bind a global middleware, we must do like so:
  // app.use(Middleware);

  // If we want to bind a global guard, we must do like so:
  // app.useGlobalGuards(new RolesGuard());
  await app.listen(3000);
}

bootstrap();

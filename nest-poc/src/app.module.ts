import { Module, NestModule, MiddlewareConsumer, RequestMethod } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CatsModule } from './domain/cats/cats.module';
import { LoggerMiddleware } from './middlewares/logger.middleware';

@Module({
  imports: [TypeOrmModule.forRoot(), CatsModule],
  controllers: [],
  providers: [],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(LoggerMiddleware).forRoutes('cats');

    // That's how we setup middlewares for specific routes.
    // consumer.apply(LoggerMiddleware).forRoutes({ path: 'cats/teste', method: RequestMethod.POST });

    // That's how we setup multiple middlewares.
    // consumer.apply(cors(), helmet(), logger).forRoutes(CatsController);

  }
}

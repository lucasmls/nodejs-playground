import { describe, it } from 'mocha'
import sinon from 'sinon'

import Product from '../../../src/models/product'
import ProductsController from '../../../src/controllers/products'

describe('Controller: Products', function () {
  const defaultProducts = [
    {
      name: 'Default product',
      description: 'product description',
      price: 100
    }
  ]

  describe('get() products', function () {
    it('should call .send with a list of products', () => {
      const request = {}
      const response = {
        send: sinon.spy()
      }

      Product.find = sinon.stub()
      Product.find.withArgs({}).resolves(defaultProducts)

      const productsController = new ProductsController(Product)

      return productsController.get(request, response).then(() => {
        sinon.assert.calledWith(response.send, defaultProducts)
      })
    })

    it('should return 400 when a error occurs', () => {
      const request = {}
      const response = {
        send: sinon.spy(),
        status: sinon.stub()
      }

      response.status.withArgs(400).returns(response)
      Product.find = sinon.stub()
      Product.find.withArgs({}).rejects({ message: 'Error' })

      const productsController = new ProductsController(Product)

      return productsController.get(request, response).then(() => {
        sinon.assert.calledWith(response.send, 'Error')
      })
    })
  })
})

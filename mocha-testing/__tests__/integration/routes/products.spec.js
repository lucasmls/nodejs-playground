import { describe, it } from 'mocha'
import { expect } from 'chai'
import supertest from 'supertest'

import app from '../../../src/server'
import Product from '../../../src/models/product'
const request = supertest(app)

describe('Routes: Products', () => {
  const defaultProduct = {
    name: 'Default product',
    description: 'product description',
    price: 100
  }
  const expectedProduct = {
    __v: 0,
    _id: '56cb91bdc3464f14678934ca',
    name: 'Default product',
    description: 'product description',
    price: 100
  }

  beforeEach(function () {
    const product = new Product(defaultProduct)
    product._id = '56cb91bdc3464f14678934ca'
    return Product.remove({}).then(() => product.save())
  })

  afterEach(function () {
    return Product.remove({})
  })

  describe('GET /products', () => {
    it('should return a list of products', done => {
      request.get('/products').end((err, res) => {
        console.log('Inside', res.body)
        expect(res.body).to.eql([expectedProduct])
        done(err)
      })
    })
  })
})

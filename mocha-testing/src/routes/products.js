import express from 'express'

import Product from '../models/product'
import ProductsController from '../controllers/products'

const router = express.Router()

const productsController = new ProductsController(Product)

router.get('/', productsController.get.bind(productsController))

export default router

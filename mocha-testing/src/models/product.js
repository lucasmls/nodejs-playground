import mongoose from 'mongoose'

const schema = new mongoose.Schema({
  name: String,
  description: String,
  price: Number
})

// Was needed to add the 'or' operator, cause it was recreating the model and
// throwing a error
export default mongoose.models.Product || mongoose.model('Product', schema)

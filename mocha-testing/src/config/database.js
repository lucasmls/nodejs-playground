require('dotenv').config()

const MONGODB_URLS = {
  production: process.env.MONGODB_URL,
  test: process.env.MONGODB_TEST_URL,
  development: process.env.MONGODB_DEVELOPMENT_URL
}

module.exports = {
  uri: MONGODB_URLS[process.env.NODE_ENV]
}

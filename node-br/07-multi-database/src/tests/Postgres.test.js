const assert = require("assert");
const Postgres = require("../database/strategies/postgres/Postgres");

const Hero = require("../database/strategies/postgres/models/Hero");
const ContextStrategy = require("../database/base/ContextStrategy");

const MOCK_HERO = {
  name: "Gavião Negro",
  power: "Flechas"
};

const MOCK_HERO_TO_UPDATE = {
  name: "Mulher Maravilha",
  power: "Chicote"
};

let context = {};
describe.only("Postgres Strategy", function() {
  this.timeout(Infinity);

  this.beforeAll(async function() {
    const connection = await Postgres.connect();
    const model = await Postgres.__defineModel(connection, Hero);
    context = new ContextStrategy(new Postgres(connection, model));

    await context.create(MOCK_HERO_TO_UPDATE);
  });

  it("PostgresSQL Connection", async () => {
    const result = await context.isConnected();
    assert.equal(result, true);
  });

  it("Should register a hero into the database", async () => {
    const result = await context.create(MOCK_HERO);
    delete result.id;
    assert.deepEqual(result, MOCK_HERO);
  });

  it("Should list a hero", async () => {
    const result = await context.find({ name: MOCK_HERO.name });
    delete result.id;
    assert.deepEqual(result, MOCK_HERO);
  });

  it("Should update a specific hero", async () => {
    const heroToUpdate = await context.find({ name: MOCK_HERO_TO_UPDATE.name });

    const newHero = {
      ...MOCK_HERO_TO_UPDATE,
      name: "Indiana Jones"
    };

    const [result] = await context.update(heroToUpdate.id, newHero);
    const updatedHero = await context.find({ id: heroToUpdate.id });

    assert.deepEqual(result, 1);
    assert.deepEqual(updatedHero.name, newHero.name);
  });

  it("Should remove a specific hero", async () => {
    const [item] = await context.index({});
    const result = await context.delete(item.id);
    assert.deepEqual(result, 1);
  });
});

const Mongoose = require("mongoose");
const ICrud = require("../../interfaces/ICrud");

const STATUS = {
  0: "disconnected",
  1: "connected",
  2: "connecting",
  3: "disconnecting"
};

class MongoDB extends ICrud {
  constructor(connection, schema) {
    super();

    this.__schema = schema;
    this.__connection = connection;
  }

  static connect() {
    Mongoose.connect(
      "mongodb://lucas:1234@localhost:27017/heroes",
      { useNewUrlParser: true },
      error => {
        if (!error) return;
        console.log("Falha na conexão!", error);
      }
    );

    const connection = Mongoose.connection;
    connection.once("open", () => console.log("Connected on MongoDB!"));
    return connection;
  }

  async isConnected() {
    const state = STATUS[this.__connection.readyState];
    if (state === "connected") return state;
    if (state !== "connecting") return state;

    await new Promise(resolve => setTimeout(resolve, 1000));
    return STATUS[this.__connection.readyState];
  }

  async index(item, skip = 0, limit = 10) {
    return this.__schema
      .find(item)
      .skip(skip)
      .limit(limit);
  }

  create(item) {
    return this.__schema.create(item);
  }

  update(id, data) {
    return this.__schema.updateOne({ _id: id }, { $set: data });
  }

  delete(id) {
    return this.__schema.deleteOne({ _id: id });
  }
}

module.exports = MongoDB;

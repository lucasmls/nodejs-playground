const Mongoose = require("mongoose");

const heroSchema = Mongoose.Schema({
  name: {
    type: String,
    required: true
  },

  power: {
    type: String,
    required: true
  },

  createdAt: {
    type: Date,
    default: new Date()
  }
});

module.exports = Mongoose.models.hero || Mongoose.model("hero", heroSchema);

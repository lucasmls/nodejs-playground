class NotImplementedException extends Error {
  constructor() {
    super("Not implemented Exception");
  }
}

class ICrud {
  connect(item) {
    throw new NotImplementedException();
  }

  index(item) {
    throw new NotImplementedException();
  }

  find(item) {
    throw new NotImplementedException();
  }

  create(item) {
    throw new NotImplementedException();
  }

  read(query) {
    throw new NotImplementedException();
  }

  update(id, item) {
    throw new NotImplementedException();
  }

  delete(id) {
    throw new NotImplementedException();
  }

  isConnected() {
    throw new NotImplementedException();
  }
}

module.exports = ICrud;

const http = require("http");

const PORT = 5000;

const server = http.createServer((req, res) => res.end("Hello HTTP!"));

server.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});

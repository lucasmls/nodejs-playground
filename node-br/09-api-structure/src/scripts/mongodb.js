docker exec -it 5feab5dd464f mongo -u lucas -p 1234 --authenticationDatabase heroes

// databases
show dbs

// muda o contexto para a database
use heroes

// listas as collections (table!?)
show collections

// create
db.heroes.insert({
  name: 'Flash',
  power: 'Speed'
})

for(let i = 0; i <= 100; i++) {
  db.heroes.insert({
    name: `Clone-${i}`,
    power: `Speed`,
  })
}

// read
db.heroes.find().limit(5).sort({ name: -1 }).pretty()
db.heroes.find().pretty()
db.heroes.findOne()
db.heroes.find({}, { power:true, _id: false })
db.heroes.count()

// update
db.heroes.find({ name: 'Flash' }).pretty()
// {
//   "_id" : ObjectId("5c6b1a2c3c706f8bc9bb9938"),
//     "name" : "Flash",
//       "power" : "Speed"
// }

// Atualiza o objeto inteiro, podendo perder informações
db.heroes.update(
  { _id: ObjectId("5c6b1a2c3c706f8bc9bb9938") },
  { name: 'Batman' }
)

// Apenas "seta" a chave value
db.heroes.update(
  { _id: ObjectId("5c6b1bf3a12438894bef50ea") },
  { $set: { name: 'Naruto' } }
)


// delete
db.heroes.remove({}) // Remove a base de dados inteira
db.heroes.remove({ _id: ObjectId("5c6b1bf3a12438894bef50ea") })

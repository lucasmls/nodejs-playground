const assert = require("assert");

const api = require("../api");
const ContextStrategy = require("../database/base/ContextStrategy");
const Postgres = require("../database/strategies/postgres/Postgres");
const User = require("../database/strategies/postgres/models/User");

let app = {};

const USER = {
  username: "lucasmls",
  password: "123457"
};

const USER_DB = {
  username: "lucasmls",
  password: "$2b$04$8CVWKlRVMYMo5fKw0jQksOSwrppZKJ0/TKr.dBm48AD3RURDLqr.y"
};

describe("Authentication of Heroes API", function() {
  this.beforeAll(async () => {
    app = await api;

    const connection = await Postgres.connect();
    const model = await Postgres.__defineModel(connection, User);
    const postgresContext = new ContextStrategy(
      new Postgres(connection, model)
    );

    const result = await postgresContext.update(null, USER_DB, true);
  });

  after(() => {
    app.stop();
  });

  it("Should receive a auth token", async () => {
    const result = await app.inject({
      method: "POST",
      url: "/login",
      payload: USER
    });

    const statusCode = result.statusCode;
    const data = JSON.parse(result.payload);

    assert.deepEqual(statusCode, 200);
    assert.ok(data.token.length > 10);
  });

  it("Should return unauthorized when a the credentials is wrong", async () => {
    const result = await app.inject({
      method: "POST",
      url: "/login",
      payload: {
        username: "lucaslsbh1",
        password: "123123123"
      }
    });

    const statusCode = result.statusCode;
    const data = JSON.parse(result.payload);

    assert.deepEqual(statusCode, 401);
    assert.deepEqual(data.error, "Unauthorized");
  });
});

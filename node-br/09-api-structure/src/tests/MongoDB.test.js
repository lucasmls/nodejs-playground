const assert = require("assert");
const MongoDB = require("../database/strategies/mongodb/MongoDB");

const HeroSchema = require("../database/strategies/mongodb/schemas/heroSchema");
const ContextStrategy = require("../database/base/ContextStrategy");

const MOCK_HERO = {
  name: "Gavião Negro",
  power: "Flechas"
};

const MOCK_HERO_DEFAULT = {
  name: `Pantera Negra-${Date.now()}`,
  power: "Garra"
};

const MOCK_HERO_TO_UPDATE = {
  name: `Patolino-${Date.now()}`,
  power: "Corridinha"
};

let HERO_TO_UPDATE_ID = null;

let context = {};
describe("MongoDB", function() {
  this.beforeAll(async function() {
    const connection = MongoDB.connect();
    context = new ContextStrategy(new MongoDB(connection, HeroSchema));

    await context.create(MOCK_HERO_DEFAULT);
    const result = await context.create(MOCK_HERO_TO_UPDATE);
    HERO_TO_UPDATE_ID = result._id;
  });

  it("MongoDB Connection", async () => {
    const result = await context.isConnected();
    const expected = "connected";
    assert.deepEqual(result, expected);
  });

  it("Register Hero", async () => {
    const { name, power } = await context.create(MOCK_HERO);
    assert.deepEqual({ name, power }, MOCK_HERO);
  });

  it("Lista heroes", async () => {
    const [{ name, power }] = await context.index({
      name: MOCK_HERO_DEFAULT.name
    });
    const result = { name, power };
    assert.deepEqual(result, MOCK_HERO_DEFAULT);
  });

  it("Update a hero", async () => {
    const result = await context.update(HERO_TO_UPDATE_ID, {
      name: "Pernalonga"
    });

    assert.deepEqual(result.nModified, 1);
  });

  it("Delete a hero", async () => {
    const result = await context.delete(HERO_TO_UPDATE_ID);
    assert.deepEqual(result.n, 1);
  });
});

const assert = require("assert");
const api = require("../api");

let app = {};
const AUTH_TOKEN =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MSwidXNlcm5hbWUiOiJsdWNhc21scyIsImlhdCI6MTU1MDk0MDMwN30.XQHCfqiTqDW-JgvFhj1E8i0Ct_vWNVHy4yXUVIUMeHE";

const headers = {
  Authorization: AUTH_TOKEN
};

const MOCK_REGISTER_HERO = {
  name: "Chapolin",
  power: "Marreta"
};

const MOCK_INITIAL_HERO = {
  name: "Bojack",
  power: "Coice"
};

let MOCK_ID = "";

describe("Heroes API", function() {
  this.beforeAll(async () => {
    app = await api;

    const result = await app.inject({
      headers,
      method: "POST",
      url: "/heroes",
      headers,
      payload: MOCK_INITIAL_HERO
    });

    const data = JSON.parse(result.payload);
    MOCK_ID = data._id;
  });

  after(() => {
    app.stop();
  });

  it("GET on /heroes should list the heroes", async () => {
    const result = await app.inject({
      headers,
      method: "GET",
      url: "/heroes?skip=0&limit=10"
    });

    const statusCode = result.statusCode;
    const payload = JSON.parse(result.payload);

    assert.deepEqual(statusCode, 200);
    assert.ok(Array.isArray(payload));
  });

  it("GET on /heroes should list 3 heroes", async () => {
    const LIMIT_QUANTITY = 3;
    const result = await app.inject({
      headers,
      method: "GET",
      url: `/heroes?skip=0&limit=${LIMIT_QUANTITY}`
    });

    const statusCode = result.statusCode;
    const payload = JSON.parse(result.payload);

    assert.deepEqual(statusCode, 200);
    assert.deepEqual(payload.length, LIMIT_QUANTITY);
  });

  it("GET on /heroes with string on param should throw an error", async () => {
    const LIMIT_QUANTITY = "string";
    const result = await app.inject({
      headers,
      method: "GET",
      url: `/heroes?skip=0&limit=${LIMIT_QUANTITY}`
    });

    const statusCode = result.statusCode;
    assert.deepEqual(statusCode, 400);
  });

  it("Should list only one item", async () => {
    const HERO_NAME = MOCK_INITIAL_HERO.name;
    const result = await app.inject({
      headers,
      method: "GET",
      url: `/heroes?skip=0&limit=1000&name=${HERO_NAME}`
    });

    const statusCode = result.statusCode;
    const payload = JSON.parse(result.payload);

    assert.deepEqual(statusCode, 200);
    assert.deepEqual(payload[0].name, HERO_NAME);
  });

  it("Should register a hero", async () => {
    const result = await app.inject({
      headers,
      method: "POST",
      url: "/heroes",
      payload: MOCK_REGISTER_HERO
    });

    const statusCode = result.statusCode;
    const { message, _id } = JSON.parse(result.payload);

    assert.ok(statusCode === 200);
    assert.notStrictEqual(_id, undefined);
    assert.deepEqual(message, "Herói cadastrado com sucesso!");
  });

  it("Should patch a hero - heroes/:id", async () => {
    const _id = MOCK_ID;

    const expected = {
      power: "Super coice"
    };

    const result = await app.inject({
      headers,
      method: "PATCH",
      url: `/heroes/${_id}`,
      payload: expected
    });

    const statusCode = result.statusCode;
    const data = JSON.parse(result.payload);

    assert.ok(statusCode === 200);
    assert.deepEqual(data.message, "Herói atualizado com sucesso!");
  });

  it("Should not patch with a incorrect id heroes/:id", async () => {
    const _id = `5c6dba8e497c0a0c104c6399`;

    const expected = {
      power: "Super coice"
    };

    const result = await app.inject({
      headers,
      method: "PATCH",
      url: `/heroes/${_id}`,
      payload: expected
    });

    const statusCode = result.statusCode;
    const data = JSON.parse(result.payload);

    assert.ok(statusCode === 412);
    assert.deepEqual(data.message, "Herói não encontrado no banco");
  });

  it("Should delete a hero - /heroes/:id", async () => {
    const _id = MOCK_ID;

    const result = await app.inject({
      headers,
      method: "DELETE",
      url: `/heroes/${_id}`
    });

    const statusCode = result.statusCode;
    const data = JSON.parse(result.payload);

    assert.ok(statusCode === 200);
    assert.deepEqual(data.message, "Herói removido com sucesso");
  });

  it("Should not delete a hero with unknown id - /heroes/:id", async () => {
    const _id = "5c6dba8e497c0a0c104c6399";

    const result = await app.inject({
      headers,
      method: "DELETE",
      url: `/heroes/${_id}`
    });

    const statusCode = result.statusCode;
    const data = JSON.parse(result.payload);

    assert.ok(statusCode === 412);
    assert.deepEqual(data.message, "Herói não encontrado no banco");
  });

  it("Should not delete a hero with invalid id - /heroes/:id", async () => {
    const _id = "ID_INVALIDO";

    const result = await app.inject({
      headers,
      method: "DELETE",
      url: `/heroes/${_id}`
    });

    const statusCode = result.statusCode;
    const data = JSON.parse(result.payload);

    assert.ok(statusCode === 500);
    assert.deepEqual(data.message, "An internal server error occurred");
  });
});

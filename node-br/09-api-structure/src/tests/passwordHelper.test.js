const assert = require("assert");

const PasswordHelper = require("../helpers/passwordHelper");

const PASSWORD = "123457";
const HASH_PASSWORD =
  "$2b$04$8CVWKlRVMYMo5fKw0jQksOSwrppZKJ0/TKr.dBm48AD3RURDLqr.y";

describe("Password helper tests", () => {
  it("Should create a hash based on a password", async () => {
    const result = await PasswordHelper.generateHash(PASSWORD);
    assert.ok(result.length > 10);
  });

  it("Should compare a hash, and its corresponding password", async () => {
    const result = await PasswordHelper.comparePassword(
      PASSWORD,
      HASH_PASSWORD
    );

    assert.ok(result);
  });
});

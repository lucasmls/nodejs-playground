const Mongoose = require("mongoose");

Mongoose.connect(
  "mongodb://lucas:1234@localhost:27017/heroes",
  { useNewUrlParser: true },
  error => {
    if (!error) return;

    console.log("Falha na conexão!", error);
  }
);

const connection = Mongoose.connection;
connection.once("open", () => console.log("Connected on MongoDB!"));

/*
  0 = disconnected
  1 = connected
  2 = connecting
  3 = disconnecting
*/

// setTimeout(() => {
//   const state = connection.readyState;
//   console.log("State: ", state);
// }, 1000);

const heroSchema = Mongoose.Schema({
  name: {
    type: String,
    required: true
  },

  power: {
    type: String,
    required: true
  },

  createdAt: {
    type: Date,
    default: new Date()
  }
});

const Hero = Mongoose.model("Hero", heroSchema);

async function main() {
  const registerResult = await Hero.create({
    name: "Indiana Jones",
    power: "Chicote"
  });

  console.log("Resultado do Register", registerResult);

  const data = await Hero.find();
  console.log("Dados: ", data);
}

main();

const BaseRoute = require("./base/baseRoute");
const Joi = require("joi");
const Boom = require("boom");

const failAction = (request, headers, error) => {
  throw error;
};

const headers = Joi.object({
  authorization: Joi.string().required()
}).unknown();

class HeroRoutes extends BaseRoute {
  constructor(db) {
    super();
    this.db = db;
  }

  index() {
    return {
      path: "/heroes",
      method: "GET",
      config: {
        tags: ["api"],
        description: "Deve listar os heróis",
        notes: "aceita paginação e pode filtrar pelo nome do herói",
        validate: {
          headers,
          // payload => body
          //  headers => header
          // params = na URL :id
          // query => ?skip=10&limit=20
          failAction,
          query: {
            skip: Joi.number()
              .integer()
              .default(0),

            limit: Joi.number()
              .integer()
              .default(10),

            name: Joi.string()
              .min(3)
              .max(100)
          }
        }
      },

      handler: (request, headers) => {
        try {
          const { skip, limit, name } = request.query;
          const query = {
            name: {
              $regex: `.*${name}*.`
            }
          };

          return this.db.index(name ? query : {}, skip, limit);
        } catch (error) {
          console.log("DEU RUIM", error);
          return Boom.internal();
        }
      }
    };
  }

  create() {
    return {
      path: "/heroes",
      method: "POST",
      config: {
        tags: ["api"],
        description: "Deve cadastrar o heroi",
        notes: "deve cadastrar heroi por nome e poder",
        validate: {
          headers,
          failAction,
          payload: {
            name: Joi.string()
              .required()
              .min(3)
              .max(100),
            power: Joi.string()
              .required()
              .min(2)
              .max(100)
          }
        }
      },

      handler: async request => {
        try {
          const { name, power } = request.payload;
          const result = await this.db.create({ name, power });
          return { message: "Herói cadastrado com sucesso!", _id: result._id };
        } catch (error) {
          console.log("DEU RUIM", error);
          return Boom.internal();
        }
      }
    };
  }

  update() {
    return {
      path: "/heroes/{id}",
      method: "PATCH",
      config: {
        tags: ["api"],
        description: "Deve atualizar o heroi pelo ID",
        notes: "pode atualizar qualquer campo",
        validate: {
          headers,
          params: {
            id: Joi.string().required()
          },
          payload: {
            name: Joi.string()
              .min(3)
              .max(100),
            power: Joi.string()
              .min(2)
              .max(100)
          }
        }
      },

      handler: async request => {
        try {
          const { id } = request.params;
          const { payload } = request;

          const data = JSON.parse(JSON.stringify(payload));

          const result = await this.db.update(id, data);

          if (result.nModified !== 1)
            return Boom.preconditionFailed("Herói não encontrado no banco");

          return {
            message: "Herói atualizado com sucesso!"
          };
        } catch (error) {
          console.log("DEU RUIM", error);
          return Boom.internal();
        }
      }
    };
  }

  delete() {
    return {
      path: "/heroes/{id}",
      method: "DELETE",
      config: {
        tags: ["api"],
        description: "Deve remover o heroi pelo ID",
        notes: "o ID deve ser válido",
        validate: {
          headers,
          failAction,
          params: {
            id: Joi.string().required()
          }
        }
      },

      handler: async request => {
        try {
          const { id } = request.params;
          const result = await this.db.delete(id);

          if (result.n !== 1)
            return Boom.preconditionFailed("Herói não encontrado no banco");

          return {
            message: "Herói removido com sucesso"
          };
        } catch (error) {
          console.log("DEU RUIM", error);
          return Boom.internal();
        }
      }
    };
  }
}

module.exports = HeroRoutes;

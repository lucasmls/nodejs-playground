const BaseRoute = require("./base/baseRoute");
const PasswordHelper = require("../helpers/passwordHelper");

const Joi = require("joi");
const Boom = require("boom");
const JWT = require("jsonwebtoken");

const failAction = (reques, headers, error) => {
  throw error;
};

const MOCK_USER = {
  username: "lucasmls",
  password: "123457"
};

class AuthRoutes extends BaseRoute {
  constructor(db, secret) {
    super();
    this.db = db;
    this.secret = secret;
  }

  login() {
    return {
      path: "/login",
      method: "POST",
      config: {
        auth: false,
        tags: ["api"],
        description: "Reliza o login",
        notes:
          "Realiza o login na aplicação com usuário e senha e recebe o token de autênticação",
        validate: {
          failAction,
          payload: {
            username: Joi.string().required(),
            password: Joi.string().required()
          }
        }
      },

      handler: async (request, headers) => {
        const { username, password } = request.payload;

        const [user] = await this.db.index({
          username: username.toLowerCase()
        });

        if (!user) {
          return Boom.unauthorized("O usuário buscado não existe.");
        }

        const passwordsMatch = PasswordHelper.comparePassword(
          password,
          user.password
        );

        if (!passwordsMatch) {
          return Boom.unauthorized("Usuário ou senha inválidos");
        }

        // if (
        //   username.toLowerCase() !== MOCK_USER.username ||
        //   password !== MOCK_USER.password
        // ) {
        //   return Boom.unauthorized();
        // }

        const token = JWT.sign(
          {
            id: 1,
            username
          },
          this.secret
        );

        return { token };
      }
    };
  }
}

module.exports = AuthRoutes;

const Sequelize = require("sequelize");

const driver = new Sequelize("heroes", "lucas", "1234", {
  host: "localhost",
  dialect: "postgres",
  quoteIdentifiers: false,
  operatorsAliases: false
});

async function main() {
  const Heroes = driver.define(
    "heroes",
    {
      id: {
        type: Sequelize.INTEGER,
        required: true,
        primaryKey: true,
        autoIncrement: true
      },

      name: {
        type: Sequelize.STRING,
        required: true
      },

      power: {
        type: Sequelize.STRING,
        required: true
      }
    },
    {
      tableName: "heroes",
      freezeTableName: true,
      timestamps: false
    }
  );

  await Heroes.sync();
  await Heroes.create({
    name: "Lanterna Verde",
    power: "Anel"
  });

  const result = await Heroes.findAll({
    raw: true,
    attributes: ["power"]
  });

  console.log(result);
}

main();

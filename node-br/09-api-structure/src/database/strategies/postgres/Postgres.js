const Sequelize = require("sequelize");
const ICrud = require("../../interfaces/ICrud");

class Postgres extends ICrud {
  constructor(connection, model) {
    super();
    this.__connection = connection;
    this.__model = model;
  }

  static async connect() {
    const connection = new Sequelize(process.env.POSTGRES_URL, {
      quoteIdentifiers: false,
      operatorsAliases: false,
      logging: false,
      ssl: process.env.SSL_DB,
      dialectOptions: {
        ssl: process.env.SSL_DB
      }
    });

    return connection;
  }

  static async __defineModel(connection, schema) {
    const model = connection.define(schema.name, schema.schema, schema.options);
    await model.sync();
    return model;
  }

  async isConnected() {
    try {
      await this.__connection.authenticate();
      return true;
    } catch (error) {
      console.error("Erro ao se conectar ao Postgres", error);
      return false;
    }
  }

  async create(item) {
    const result = await this.__model.create(item);
    const data = result.get({
      plain: true
    });

    return data;
  }

  async index(item) {
    return this.__model.findAll({
      where: item,
      raw: true
    });
  }

  async find(item) {
    return this.__model.findOne({
      where: item,
      raw: true
    });
  }

  async update(id, item, upsert = false) {
    const fnc = upsert ? "upsert" : "update";

    return await this.__model[fnc](item, {
      where: { id }
    });
  }

  async delete(id) {
    const query = id ? { id } : {};
    return this.__model.destroy({
      where: query
    });
  }
}

module.exports = Postgres;

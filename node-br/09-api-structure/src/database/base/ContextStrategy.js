const ICrud = require("../interfaces/ICrud");

class ContextStrategy extends ICrud {
  constructor(strategy) {
    super();
    this.__database = strategy;
  }

  static connect() {
    return this.__database.connect();
  }

  isConnected() {
    return this.__database.isConnected();
  }

  index(item, skip, limit) {
    return this.__database.index(item, skip, limit);
  }

  create(item) {
    return this.__database.create(item);
  }

  find(query) {
    return this.__database.find(query);
  }

  update(id, item, upsert) {
    return this.__database.update(id, item, upsert);
  }

  delete(id) {
    return this.__database.delete(id);
  }
}

module.exports = ContextStrategy;

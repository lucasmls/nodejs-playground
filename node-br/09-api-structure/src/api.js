const { config } = require("dotenv");
const path = require("path");

const env = process.env.NODE_ENV || "dev";
const configPath = path.join(__dirname, "./config", `.env.${env}`);
config({ path: configPath });

const Hapi = require("Hapi");
const HapiSwagger = require("hapi-swagger");
const Inert = require("inert");
const Vision = require("vision");
const HapiJWT = require("hapi-auth-jwt2");

const ContextStrategy = require("./database/base/ContextStrategy");
const MongoDB = require("./database/strategies/mongodb/MongoDB");
const HeroSchema = require("./database/strategies/mongodb/schemas/heroSchema");
const Postgres = require("./database/strategies/postgres/Postgres");
const User = require("./database/strategies/postgres/models/User");

const HeroRoutes = require("./routes/heroRoutes");
const AuthRoutes = require("./routes/authRoutes");

const app = new Hapi.Server({
  port: process.env.PORT
});

const JWT_SECRET = process.env.JWT_SECRET;

const mapRoutes = (instance, methods) => {
  return methods.map(method => instance[method]());
};

async function main() {
  const mongoDBConnection = MongoDB.connect();
  const mongoDBcontext = new ContextStrategy(
    new MongoDB(mongoDBConnection, HeroSchema)
  );

  const postgresConnection = await Postgres.connect();
  const postgresContext = new ContextStrategy(
    new Postgres(
      postgresConnection,
      await Postgres.__defineModel(postgresConnection, User)
    )
  );

  const swaggerOptions = {
    info: {
      title: "Heroes API - #node",
      version: "v1.0"
    },
    lang: "pt"
  };

  await app.register([
    HapiJWT,
    Vision,
    Inert,
    {
      plugin: HapiSwagger,
      options: swaggerOptions
    }
  ]);

  app.auth.strategy("jwt", "jwt", {
    key: JWT_SECRET,
    // options: {
    //   expiresIn: 20
    // }
    validate: async (data, request) => {
      // Verifica no banco se o usuário ainda é válido no sistema

      const [user] = await postgresContext.index({
        username: data.username.toLowerCase(),
        id: data.id
      });

      if (!user) {
        return {
          isValid: false
        };
      }

      return {
        isValid: true // false
      };
    }
  });

  app.auth.default("jwt");
  app.route([
    ...mapRoutes(new HeroRoutes(mongoDBcontext), HeroRoutes.methods()),
    ...mapRoutes(
      new AuthRoutes(postgresContext, JWT_SECRET),
      AuthRoutes.methods()
    )
  ]);

  await app.start();
  console.log(`API running on port 5000!`);
  return app;
}

module.exports = main();

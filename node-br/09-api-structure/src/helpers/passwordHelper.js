const bcrypt = require("bcrypt");
const { promisify } = require("util");

const asyncBcryptHash = promisify(bcrypt.hash);
const asyncBcryptCompare = promisify(bcrypt.compare);
const SALT = parseInt(process.env.JWT_SALT); // Nível de complexidade da geração da hash

class PasswordHelper {
  static generateHash(password) {
    return asyncBcryptHash(password, SALT);
  }

  static comparePassword(password, hash) {
    return asyncBcryptCompare(password, hash);
  }
}

module.exports = PasswordHelper;

const EventEmitter = require("events");

class MyEmitter extends EventEmitter {}

const myEmitter = new MyEmitter();
const eventName = "user:click";

// myEmitter.on(eventName, click => {
//   console.log(`um usuário clicou: ${click}`);
// });

// myEmitter.emit(eventName, "na barra de rolagem");

// let count = 0;
// setInterval(() => {
//   myEmitter.emit(eventName, `no botão ${count++} vezes`);
// }, 1000);

const stdin = process.openStdin();
stdin.addListener("data", value => {
  console.log(`Você digitou: ${value.toString().trim()}`);
});

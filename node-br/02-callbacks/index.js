const util = require("util");

const getAsyncAddress = util.promisify(getAddress);

function getUser() {
  return new Promise(function resolvePromise(resolve, reject) {
    setTimeout(function() {
      // return reject(new Error("DEU RUIM DE VERDADE"));
      return resolve({
        id: 1,
        name: "Lucas",
        birthday: new Date()
      });
    }, 1000);
  });
}

function getPhone(userID, callback) {
  return new Promise(function resolvePromise(resolve, reject) {
    setTimeout(function() {
      return resolve({
        phone: "4321-4321",
        ddd: 31
      });
    }, 2000);
  });
}

function getAddress(userID, callback) {
  setTimeout(function() {
    return callback(null, {
      street: "dos bobos",
      number: 0
    });
  }, 2000);
}

async function main() {
  try {
    console.time("time-promise");
    const user = await getUser();
    // const phone = await getPhone(user.id);
    // const address = await getAsyncAddress(user.id);
    const result = await Promise.all([
      getPhone(user.id),
      getAsyncAddress(user.id)
    ]);
    const phone = result[0];
    const address = result[1];
    console.timeEnd("time-promise");
    console.log({
      user,
      phone,
      address
    });
  } catch (error) {}
}

main();

// const userPromise = getUser();
// userPromise
//   .then(function(user) {
//     return getPhone(user.id).then(function(phone) {
//       return { user, phone };
//     });
//   })
//   .then(function(result) {
//     const address = getAsyncAddress(result.user.id);
//     return address.then(function(address) {
//       return {
//         user: result.user,
//         phone: result.phone,
//         address
//       };
//     });
//   })
//   .then(function(result) {
//     console.log("Result", result);
//   })
//   .catch(function(error) {
//     console.error("DEU RUIM", error);
//   });

// getUser(function resolveUser(error, user) {
//   if (error) {
//     console.error(error);
//     return;
//   }

//   getPhone(user.id, function resolvePhone(error1, phone) {
//     if (error1) {
//       console.error(error1);
//       return;
//     }

//     getAddress(user.id, function resolveAddress(error2, address) {
//       if (error2) {
//         console.error(error2);
//         return;
//       }
//     });
//   });
// });

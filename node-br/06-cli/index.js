const Commander = require("commander");
const Database = require("./database");
const Hero = require("./Hero");

async function main() {
  Commander.version("v1")
    .option("-n, --name [value]", "Nome do herói")
    .option("-p, --power [value]", "Poder do herói")
    .option("-i, --id [value]", "ID do herói")

    .option("-r, --register", "Cadastrar um herói")
    .option("-l, --list", "Listar um herói")
    .option("-d, --delete", "Remove um heroi pelo id")
    .option("-u, --update [value]", "Atualiza um heroi pelo id")
    .parse(process.argv);

  const hero = new Hero(Commander);

  try {
    if (Commander.register) {
      const result = Database.registerHero(hero);
      if (!result) {
        console.error("Erro ao cadastrar o heroi.");
        return;
      }
      console.log("Heroi cadastrado com sucesso!");
    }

    if (Commander.list) {
      const result = await Database.getHero();
      console.log(result);
      return;
    }

    if (Commander.delete) {
      const result = await Database.removeHero(hero.id);
      if (!result) {
        console.error("Não foi possível remover o heroi.");
        return;
      }

      console.log("Heroi removido com sucesso");
      return;
    }

    if (Commander.update) {
      const idToUpdate = parseInt(Commander.update);
      const updatedHero = JSON.parse(JSON.stringify(hero));
      const result = Database.updateHero(idToUpdate, updatedHero);
      if (!result) {
        console.error("Não foi possível atualizar o heroi.");
        return;
      }

      console.log("Heroi atualizado com sucesso.");
    }
  } catch (error) {
    console.error("DEU RUIM", error);
  }
}

main();

const { readFile, writeFile } = require("fs");
const { promisify } = require("util");

// Outra forma de ler o arquivo .json seria apenas usar o require
// const jsonFile = require('.herois.json')

const asyncReadFile = promisify(readFile);
const asyncWriteFile = promisify(writeFile);

class Database {
  constructor() {
    this.FILE_NAME = "herois.json";
  }

  async getFileData() {
    const file = await asyncReadFile(this.FILE_NAME, "utf8");
    return JSON.parse(file.toString());
  }

  async writeFile(data) {
    await asyncWriteFile(this.FILE_NAME, JSON.stringify(data));
    return true;
  }

  async registerHero(hero) {
    const data = await this.getFileData();
    const id = hero.id <= 2 ? hero.id : Date.now();
    const heroWithID = { ...hero, id };
    const finalData = [...data, heroWithID];
    return await this.writeFile(finalData);
  }

  async getHero(id) {
    const data = await this.getFileData();
    const filteredData = data.filter(hero => (id ? hero.id === id : true));
    return filteredData;
  }

  async removeHero(id) {
    if (!id) {
      return await this.writeFile([]);
    }

    const data = await this.getFileData();
    const heroIndex = data.findIndex(hero => hero.id === parseInt(id));
    if (heroIndex === -1)
      throw new Error("O heroi informado não existe na nossa base de dados.");
    data.splice(heroIndex, 1); // Removendo o heroi com o ID passado
    return await this.writeFile(data);
  }

  async updateHero(id, dataToUpdate) {
    const data = await this.getFileData();
    const heroIndex = data.findIndex(hero => hero.id === parseInt(id));

    if (heroIndex === -1)
      throw new Error("O heroi informado não existe na nossa base de dados.");

    const actualHero = data[heroIndex];
    const updatedHero = {
      ...actualHero,
      ...dataToUpdate
    };
    data.splice(heroIndex, 1);
    return this.writeFile([...data, updatedHero]);
  }
}

module.exports = new Database();

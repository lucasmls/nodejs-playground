const { deepEqual, ok } = require("assert");
const database = require("./database");

const DEFAULT_HERO = {
  id: 1,
  name: "Flash",
  power: "Speed"
};

const DEFAULT_UPDATE_HERO = {
  id: 2,
  name: "Batman",
  power: "Money"
};

describe("Suite de manipulação de Herois", () => {
  before(async () => {
    await database.registerHero(DEFAULT_HERO);
    await database.registerHero(DEFAULT_UPDATE_HERO);
  });

  it("deve pesquisar um heroi usando arquivos", async () => {
    const expected = DEFAULT_HERO;
    const [result] = await database.getHero(expected.id);
    deepEqual(result, expected);
  });

  it("deve cadastrar um heroi, usando arquivos", async () => {
    const expected = {
      id: 3,
      name: "Iron Man",
      power: "Money"
    };
    await database.registerHero(expected);
    const [actual] = await database.getHero(expected.id);
    deepEqual(actual, expected);
  });

  it("deve remover um heroi do arquivo baseado pelo o ID passado", async () => {
    const expected = true;
    const result = await database.removeHero(DEFAULT_HERO.id);
    deepEqual(result, expected);
  });

  it("deve atualizar um heroi baseado pelo ID passado", async () => {
    const expected = {
      ...DEFAULT_UPDATE_HERO,
      name: "Lanterna Verde",
      power: "Energia do Anel"
    };

    const newHeroData = {
      name: "Lanterna Verde",
      power: "Energia do Anel"
    };

    await database.updateHero(DEFAULT_UPDATE_HERO.id, newHeroData);
    const [result] = await database.getHero(DEFAULT_UPDATE_HERO.id);
    deepEqual(result, expected);
  });
});

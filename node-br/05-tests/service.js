const axios = require("axios");

const API_URL = `https://swapi.co/api/people`;

const getPeople = async name => {
  const url = `${API_URL}/?search=${name}&format=json`;
  try {
    const result = await axios.get(url);
    return result.data.results.map(mapPeoples);
  } catch (error) {
    console.error("DEU RUIM", error);
  }
};

const mapPeoples = people => ({
  nome: people.name,
  altura: people.height
});

module.exports = {
  getPeople,
  API_URL
};

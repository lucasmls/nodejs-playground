const { getPeople } = require("./service");

Array.prototype.myFilter = function(callback) {
  const list = [];
  for (index in this) {
    const result = callback(this[index], index, this);
    if (!result) continue;
    list.push(this[index]);
  }
  return list;
};

async function main() {
  try {
    const { results } = await getPeople("a");
    // const larsFamily = results.filter(function(item) {
    //   const isLar = item.name.toLowerCase().indexOf("lars") !== -1;
    //   return isLar;
    // });

    const larsFamily = results.myFilter(
      item => item.name.toLowerCase().indexOf("lars") !== -1
    );

    const names = larsFamily.map(person => person.name);
    console.log(names);
  } catch (error) {
    console.error("DEU RUIM", error);
  }
}

main();

const service = require("./service");

Array.prototype.myMap = function(callback) {
  const newMappedArray = [];
  for (let index = 0; index < this.length; index++) {
    const result = callback(this[index], index);
    newMappedArray.push(result);
  }

  return newMappedArray;
};

async function main() {
  try {
    const result = await service.getPeople("a");
    // Foreach
    // const names = [];
    // result.results.forEach(function(item) {
    //   names.push(item.name);
    // });

    // Map
    // const names = result.results.map(function(person) {
    //   return person.name;
    // });
    // const names = result.results.map(person => person.name);
    const names = result.results.myMap(function(person) {
      return person.name;
    });
    console.log(names);
  } catch (error) {}
}

main();

const { getPeople } = require("./service");

Array.prototype.myReduce = function(callback, initialValue) {
  let accumulator = typeof initialValue !== undefined ? initialValue : this[0];
  for (let index = 0; index < this.length; index++) {
    accumulator = callback(accumulator, this[index], this);
  }
  return accumulator;
};

async function main() {
  try {
    const { results } = await getPeople("a");
    const heights = results.map(item => parseInt(item.height));
    const sumOfHeights = heights.reduce((acc, item) => acc + item, 0);
    console.log(sumOfHeights);

    const meetups = [["PHPMG", "DevStarter"], ["Femug MG", "Front-end BH"]];
    const allMeetups = meetups
      .myReduce((acc, meetup) => acc.concat(meetup), [])
      .join(", ");
    console.log(allMeetups);
  } catch (error) {
    console.error("DEU RUIM", error);
  }
}

main();

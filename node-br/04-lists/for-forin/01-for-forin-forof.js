const service = require("./service");

const main = async () => {
  try {
    const result = await service.getPeople("a");
    const names = [];

    console.time("for");
    for (let index = 0; index < result.results.length - 1; index++) {
      const person = result.results[index];
      names.push(person.name);
    }
    console.timeEnd("for");

    console.time("for-in");
    for (let index in result.results) {
      const person = result.results[index];
      names.push(person.name);
    }
    console.timeEnd("for-in");

    console.time("for-of");
    for (person of result.results) {
      names.push(person.name);
    }
    console.timeEnd("for-of");

    console.log(names);
  } catch (error) {
    console.error(`Erro interno: ${error}`);
  }
};

main();

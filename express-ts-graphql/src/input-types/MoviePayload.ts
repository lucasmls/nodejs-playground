import { InputType, Field, Int } from "type-graphql";

@InputType()
export class MoviePayload {

  @Field()
  title: string;

  @Field(() => Int)
  minutes: number;
}
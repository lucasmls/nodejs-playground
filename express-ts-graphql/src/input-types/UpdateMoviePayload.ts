import { InputType, Field, Int } from "type-graphql";

@InputType()
export class UpdateMoviePayload {

  @Field(() => String, { nullable: true })
  title?: string;

  @Field(() => Int, { nullable: true })
  minutes?: number;
}
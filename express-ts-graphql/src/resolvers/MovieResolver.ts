import { Resolver, Mutation, Arg, Query, Int } from "type-graphql";
import Movie from "../entity/Movie";
import { MoviePayload } from "../input-types/MoviePayload";
import { UpdateMoviePayload } from "../input-types/UpdateMoviePayload";

@Resolver()
export class MovieResolver {

  @Mutation(() => Movie)
  async createMovie(
    @Arg('payload', () => MoviePayload) payload: MoviePayload
  ) {
    const movie = await Movie.create(payload).save()
    return movie
  }

  @Mutation(() => Boolean)
  async updateMovie(
    @Arg('id', () => Int) id: number,
    @Arg('payload', () => UpdateMoviePayload) payload: UpdateMoviePayload
  ) {
    await Movie.update({ id }, payload)
    return true
  }

  @Mutation(() => Boolean)
  async deleteMovie(
    @Arg('id', () => Int) id: number
  ) {
    await Movie.delete({ id })
    return true
  }

  @Query(() => [Movie])
  movies () {
    return Movie.find()
  }
}

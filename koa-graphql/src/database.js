const mongoose = require("mongoose");

const initDB = () => {
  // @TODO => Add DB connection string into .env
  mongoose.connect("mongodb://localhost:27017/koa-graphql", {
    useNewUrlParser: true
  });

  mongoose.connection.once("open", () => {
    console.log("connected to database");
  });
};

module.exports = initDB;

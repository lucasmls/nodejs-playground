const app = require("./server");

app.listen(process.env.PORT || 3000);

app.on("error", err => {
  log.error("server error", err);
});

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const ProductSchema = Schema({
  name: String,
  by_company: String,
  price: Number,
  release_date: Date
});

module.exports = mongoose.model("Product", ProductSchema);

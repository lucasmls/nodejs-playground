const { GraphQLObjectType, GraphQLString } = require("graphql");

const Product = require("../models/Product");
const productGraphQLType = require("./productType");

const Mutation = new GraphQLObjectType({
  name: "Mutation",
  fields: {
    registerProduct: {
      type: productGraphQLType,

      args: {
        name: { type: GraphQLString },
        release_date: { type: GraphQLString },
        by_company: { type: GraphQLString },
        price: { type: GraphQLString }
      },

      resolve: async (parent, args) => {
        const product = new Product({
          name: args.name,
          release_date: args.release_date,
          by_company: args.by_company,
          price: args.price
        });

        await product.save();

        return product;
      }
    },

    updateProduct: {
      type: productGraphQLType,

      args: {
        id: { type: GraphQLString },
        name: { type: GraphQLString },
        release_date: { type: GraphQLString },
        by_company: { type: GraphQLString },
        price: { type: GraphQLString }
      },

      resolve: async (parent, args) => {
        const product = await Product.findByIdAndUpdate(args.id, args);
        return product;
      }
    },

    removeProduct: {
      type: productGraphQLType,

      args: {
        id: { type: GraphQLString }
      },

      resolve: async (parent, args) => {
        const product = await Product.findOneAndRemove(args.id);
        return product;
      }
    }
  }
});

module.exports = Mutation;

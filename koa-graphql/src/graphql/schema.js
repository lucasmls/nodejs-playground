const { GraphQLSchema, GraphQLObjectType, GraphQLString } = require("graphql");

const mutations = require("./mutation");

const Product = require("../models/Product");
const productGraphQLType = require("./productType");

const RootQuery = new GraphQLObjectType({
  name: "RootQueryType",

  fields: {
    product: {
      type: productGraphQLType,
      args: { id: { type: GraphQLString } },
      resolve: (parent, args) => {
        const { id } = args;
        return Product.findById(id);
      }
    }
  }
});

module.exports = new GraphQLSchema({
  query: RootQuery,
  mutation: mutations
});
